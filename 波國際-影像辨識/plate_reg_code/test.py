import os
from Alpr import ALPR
import cv2

path = "img/"
d = os.listdir(path)
alpr = ALPR()

for name in d:
    img = cv2.imread(path+name)
    num,acc = alpr.alpr(img_source=img)
    cv2.imshow("t",img)
    print(num,acc)
    cv2.waitKey(0)