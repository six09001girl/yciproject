from Alpr import ALPR
from configparser import ConfigParser
from socket import socket

#讀取properies資訊
config = ConfigParser()
config.read('string.properties')
ip = config.get('remote', 'recognition.ip') #socket ip
port = config.get('remote', 'recognition.port') #socket port
config.clear()

s = socket()
s.bind((ip, int(port)))
#socket開啟監聽
print("Start listening at %s:%s"%(ip,port))
s.listen(1)
c,addr = s.accept()

alpr = ALPR()
while True:
    #接受資料 圖片位置
    imgPath = c.recv(1024).decode()
    plate = ""
    if(len(imgPath)>0):
        print(imgPath)
        plate,acc = alpr.alpr(imgPath)
    c.send((plate+","+str(acc)+"\r\n").encode())
