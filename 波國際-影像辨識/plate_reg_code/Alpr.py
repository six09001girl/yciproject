from Char_detect import Char_detect
from Char_reg import Char_reg
from Wpod_net import WPOD
import cv2
import configparser

class ALPR():
	def __init__(self):
		self.get_model_ini("string.properties")
		self.plate_detect = WPOD(self.conf['model_path']['plate_detection'])
		self.char_detect  = Char_detect(self.conf['model_path']['char_detection'])
		self.char_reg     = Char_reg(self.conf['model_path']['char_recognition'])

	def get_model_ini(self,ini_path):
		conf = configparser.ConfigParser()
		conf.read(ini_path, encoding='utf-8')
		self.conf = conf

	def find_plates(self, img):
		ratio = float(max(img.shape[:2]))/min(img.shape[:2])
		side  = int(ratio*288.)
		bound_dim = min(side + (side%(2**4)),608)
		L,TLps,ptsXY = self.plate_detect.find_plate(img,bound_dim,(480,160))
		return TLps

	def find_chars(self, plate_imgs):
		chars = []
		for plate in plate_imgs:
			chars.append(self.char_detect.detect_image(plate))
		return chars

	def chars_to_text(self, chars_imgs):
		text = ""
		p = 0.
		for chars in chars_imgs:
			acc = 0.
			tmp = ""
			for c in chars:
				t,a = self.char_reg.reg(c)
				acc += a
				tmp += t
				'''
				print(tmp)
				cv2.imshow("char",c)
				cv2.waitKey(0)
				'''
			try:
				acc /= len(tmp)
			except:
				acc = 0.
			tmp = self.plate_rule_fix(tmp)
			if(len(tmp)>len(text)):
				text = tmp
				p    = acc
		return text,p
	
	def plate_rule_fix(self, text):
		text = text.replace('_','')
		newStr = text
		if(len(text)>6):
			fstr = text[:3]
			fstr = fstr.replace('0','D')
			fstr = fstr.replace('4','A')
			fstr = fstr.replace('3','B')
			fstr = fstr.replace('2','Z')
			fstr = fstr.replace('1','T')
			fstr = fstr.replace('5','S')
			fstr = fstr.replace('6','G')
			fstr = fstr.replace('7','T')
			fstr = fstr.replace('8','B')
			fstr = fstr.replace('9','S')
			tstr = text[3:]
			tstr = tstr.replace('A','')
			tstr = tstr.replace('B','8')
			tstr = tstr.replace('C','0')
			tstr = tstr.replace('D','0')
			tstr = tstr.replace('E','')
			tstr = tstr.replace('F','')
			tstr = tstr.replace('G','6')
			tstr = tstr.replace('H','')
			tstr = tstr.replace('J','1')
			tstr = tstr.replace('K','')
			tstr = tstr.replace('L','1')
			tstr = tstr.replace('M','')
			tstr = tstr.replace('N','')
			tstr = tstr.replace('P','')
			tstr = tstr.replace('Q','0')
			tstr = tstr.replace('R','')
			tstr = tstr.replace('S','5')
			tstr = tstr.replace('T','1')
			tstr = tstr.replace('U','0')
			tstr = tstr.replace('V','')
			tstr = tstr.replace('W','')
			tstr = tstr.replace('X','')
			tstr = tstr.replace('Y','1')
			tstr = tstr.replace('Z','2')
			newStr = fstr + tstr
		if len(newStr) < 4:
			newStr = ""
		return newStr

	def alpr(self, img_path="", img_source=""):
		if(img_source!=""):
			img = img_source
		else:
			img = cv2.imread(img_path)
		self.plates = self.find_plates(img)
		self.chars  = self.find_chars(self.plates)
		self.text, self.acc = self.chars_to_text(self.chars)
		
		if(len(self.text)<6 and img.shape[0]<=1080):
			img = cv2.resize(img,(1650,840))
			self.plates = self.find_plates(img)
			self.chars  = self.find_chars(self.plates)
			self.text , self.acc = self.chars_to_text(self.chars)
		
		return self.text, self.acc
