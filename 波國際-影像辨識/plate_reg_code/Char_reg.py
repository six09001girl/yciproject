import onnxruntime as rt
from onnxruntime.capi.onnxruntime_pybind11_state import InvalidGraph
import numpy as np
import os
from cv2 import cvtColor,COLOR_BGR2GRAY,resize,equalizeHist,createCLAHE

class Char_reg():
	def __init__(self,model_path):
		self.alphabet = "0123456789ABCDEFGHIJKLMNPQRSTUVWXYZ_"
		self.model = rt.InferenceSession(model_path)

	def reg(self, char_img):
		char_img = cvtColor(char_img,COLOR_BGR2GRAY)
		char_img = resize(char_img,(32,64))
		img = np.array(char_img,dtype=np.float32)/255.
		img = np.expand_dims([img], axis=3)

		input_name = self.model.get_inputs()[0].name
		res  = self.model.run(None, {input_name: img})
		prob = res[0]
		acc  = max(prob[0])
		y    = self.alphabet[np.argmax(prob)]
		return y,acc
