import onnxruntime
import numpy as np
import cv2

class Char_detect():
    def __init__(self,model_path):
        self.model = onnxruntime.InferenceSession(model_path)

    def detect_image(self,img):
        image_data = np.array(img, dtype='float32')
        image_data /= 255.
        image_data = np.expand_dims(image_data, 0)  # Add batch dimension.
        image_data_onnx = np.transpose(image_data, [0, 3, 1, 2])
        feed_f = dict(zip(['input_1', 'image_shape'],(image_data_onnx, np.array([img.shape[0], img.shape[1]], dtype='float32').reshape(1, 2))))
        
        all_boxes, _, indices = self.model.run(None, input_feed=feed_f)

        out_boxes = []
        for idx_ in indices[0]:
            idx_1 = (idx_[0], idx_[2])
            out_boxes.append(all_boxes[idx_1])
        
        chars = []
        #boxes = []

        for c in list(sorted(out_boxes,key=lambda s: s[1])):
            top, left, bottom, right = c
            #boxes.append(box)
            top = max(0, np.floor(top + 0.5).astype('int32')-1)
            left = max(0, np.floor(left + 0.5).astype('int32')-1)
            bottom = min(img.shape[0], np.floor(bottom + 0.5).astype('int32')+1)
            right = min(img.shape[1], np.floor(right + 0.5).astype('int32')+1)
            chars.append(img[top:bottom,left:right])
        
        return chars