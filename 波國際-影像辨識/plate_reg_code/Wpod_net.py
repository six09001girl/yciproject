import onnxruntime as rt
from onnxruntime.capi.onnxruntime_pybind11_state import InvalidGraph

import numpy as np
import cv2
import time

from os.path import splitext

from Wpod_label import Label,DLabel

class WPOD():
    def im2single(self,img):
        assert(img.dtype == 'uint8')
        return img.astype('float32')/255.
    
    #建構子
    def __init__(self,model_path):
        self.model = rt.InferenceSession(model_path)
    
    #資料正規畫
    def reconstruct(self,Iorig,oimg,I,Y,out_size,threshold=.9):
        net_stride 	= 2**4
        side 		= ((208. + 40.)/2.)/net_stride # 7.75

        Probs = Y[...,0]
        Affines = Y[...,2:]
        rx,ry = Y.shape[:2]
        ywh = Y.shape[1::-1]
        iwh = np.array(I.shape[1::-1],dtype=float).reshape((2,1))

        xx,yy = np.where(Probs>threshold)

        WH = self.getWH(I.shape)
        MN = WH/net_stride

        vxx = vyy = 0.5 #alpha

        base = lambda vx,vy: np.matrix([[-vx,-vy,1.],[vx,-vy,1.],[vx,vy,1.],[-vx,vy,1.]]).T
        labels = []

        for i in range(len(xx)):
            y,x = xx[i],yy[i]
            affine = Affines[y,x]
            prob = Probs[y,x]

            mn = np.array([float(x) + .5,float(y) + .5])

            A = np.reshape(affine,(2,3))
            A[0,0] = max(A[0,0],0.)
            A[1,1] = max(A[1,1],0.)

            pts = np.array(A*base(vxx,vyy)) #*alpha
            pts_MN_center_mn = pts*side
            pts_MN = pts_MN_center_mn + mn.reshape((2,1))

            pts_prop = pts_MN/MN.reshape((2,1))

            labels.append(DLabel(0,pts_prop,prob))

        final_labels = self.nms(labels,.1)
        TLps  = []
        ptsXY = []
        if len(final_labels):
            final_labels.sort(key=lambda x: x.prob(), reverse=True)
            for i,label in enumerate(final_labels):
                t_ptsh 	= self.getRectPts(0,0,out_size[0],out_size[1])
                ptsh 	= np.concatenate((label.pts*self.getWH(Iorig.shape).reshape((2,1)),np.ones((1,4))))
                H 		= self.find_T_matrix(ptsh,t_ptsh)
                Ilp     = ""
                Ilp 	= cv2.warpPerspective(oimg,H,out_size,borderMode=cv2.BORDER_CONSTANT, borderValue=(0, 0, 0))
                ptsXY.append(ptsh)
                TLps.append(Ilp)

        return final_labels,TLps,ptsXY

    def find_plate(self,oimg,max_dim,out_size):
        img = self.im2single(oimg)
        threshold = .5
        net_step = 2**4

        min_dim_img = min(img.shape[:2])
        factor 		= float(max_dim)/min_dim_img

        w,h = (np.array(img.shape[1::-1],dtype=float)*factor).astype(int).tolist()
        w += (w%net_step!=0)*(net_step - w%net_step)
        h += (h%net_step!=0)*(net_step - h%net_step)
        resized = cv2.resize(img,(w,h))

        temp = resized.copy()
        temp = temp.reshape((1,temp.shape[0],temp.shape[1],temp.shape[2]))

        start 	= time.time()
        input_name = self.model.get_inputs()[0].name
        res = self.model.run(None, {input_name: temp})
        Yr 		= np.squeeze(res)
        elapsed = time.time() - start

        L,TLps,ptsXY = self.reconstruct(img,oimg,resized,Yr,out_size,threshold)

        return L,TLps,ptsXY

    #取得長寬
    def getWH(self,shape):
	    return np.array(shape[1::-1]).astype(float)
        
    def IOU(self,tl1,br1,tl2,br2):
        wh1,wh2 = br1-tl1,br2-tl2
        assert((wh1>=.0).all() and (wh2>=.0).all())
        
        intersection_wh = np.maximum(np.minimum(br1,br2) - np.maximum(tl1,tl2),0.)
        intersection_area = np.prod(intersection_wh)
        area1,area2 = (np.prod(wh1),np.prod(wh2))
        union_area = area1 + area2 - intersection_area
        return intersection_area/union_area

    def IOU_labels(self,l1,l2):
	    return self.IOU(l1.tl(),l1.br(),l2.tl(),l2.br())

    def IOU_centre_and_dims(self,cc1,wh1,cc2,wh2):
        return self.IOU(cc1-wh1/2.,cc1+wh1/2.,cc2-wh2/2.,cc2+wh2/2.)


    def nms(self,Labels,iou_threshold=.5):

        SelectedLabels = []
        Labels.sort(key=lambda l: l.prob(),reverse=True)
        
        for label in Labels:

            non_overlap = True
            for sel_label in SelectedLabels:
                if self.IOU_labels(label,sel_label) > iou_threshold:
                    non_overlap = False
                    break

            if non_overlap:
                SelectedLabels.append(label)

        return SelectedLabels

    def getRectPts(self,tlx,tly,brx,bry):
	    return np.matrix([[tlx,brx,brx,tlx],[tly,tly,bry,bry],[1.,1.,1.,1.]],dtype=float)

    def find_T_matrix(self,pts,t_pts):
        A = np.zeros((8,9))
        for i in range(0,4):
            xi  = pts[:,i]
            xil = t_pts[:,i]
            xi  = xi.T
            
            A[i*2,   3:6] = -xil[2]*xi
            A[i*2,   6: ] =  xil[1]*xi
            A[i*2+1,  :3] =  xil[2]*xi
            A[i*2+1, 6: ] = -xil[0]*xi

        
        [U,S,V] = np.linalg.svd(A)
        H = V[-1,:].reshape((3,3))

        return H
