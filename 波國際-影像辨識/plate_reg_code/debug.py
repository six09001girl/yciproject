from Alpr import ALPR
import cv2
import os

debug_imgs_path = "./img/"
debug_imgs = os.listdir(debug_imgs_path)

alpr = ALPR()

for img in debug_imgs:
	text,acc = alpr.alpr(debug_imgs_path+img)
	print(text,acc,img)
	
	for i, p in enumerate(alpr.plates):
		cv2.imshow("plate"+str(i),p)
	
	for i, char in enumerate(alpr.chars):
		for j, c in enumerate(char):
			cv2.imshow("char"+str(i)+str(j),c)
	
	i = cv2.imread(debug_imgs_path+img)
	cv2.imshow("img",i)
	cv2.waitKey(0)
	cv2.destroyAllWindows()